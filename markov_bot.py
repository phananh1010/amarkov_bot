__author__ = "Anh P. Nguyen"

import numpy
import scipy.sparse
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.externals import joblib

class AMarkovBot:
    #use bigram to predict next word
    _file_path = ''
    _path_features = ''
    _path_frequency = ''
    _ngram_range = (2,2)
    _frequency = []
    _features = []
    
    _S_END = 's_end'
    _S_BEGIN = 's_begin'
    
    def __init__(self, use_model=False, file_path='data_jokes', model_features='model_mbot_features', model_frequency='model_mbot_frequency', ngram_range=(2,2)):
        self._ngram_range = ngram_range
        if use_model == False: #then file_path must be data_path, type string
            self._file_path = file_path
            data_raw = open(self._file_path, 'r').read()
            data = self._S_END + ' ' + data_raw.lower().replace('"', '').replace('\n', ' ').replace('.', '  ' + self._S_END + '.').replace(',', '  ' + self._S_END + '.')
            sentences = data.split('.')
        
            cvect = CountVectorizer(ngram_range=self._ngram_range, max_features=50000 , encoding='utf-8', tokenizer=None)
            X = cvect.fit_transform([data])
        
            self._features = cvect.get_feature_names()
            self._frequency = [scipy.sparse.csr_matrix.sum(X.getcol(i)) for i in range(len(self._features))]
        else:
            #file_path must be list of two name, feature&frequency
            self._path_features = model_features
            self._path_frequency = model_frequency
            self._features = joblib.load(self._path_features)
            self._frequency = joblib.load(self._path_frequency)
    
    def save(self, frequency_name='model_mb_frequency', features_name = 'model_mb_features'):
        joblib.dump(self._frequency, frequency_name)
        joblib.dump(self._features, features_name)
    
    
    #for each word, create distribution list of next word
    def _get_dist_list(self, word):
        #input: a word, a frequency for (word_i, word_j), feature list
        #output: cumulative distribution list for of word_j that has word_i == word
        result = []
        sum = 0

        for i,item in enumerate(self._frequency):
            if self._features[i].split(' ')[0] == word:
                result.append([i, sum])
                sum += self._frequency[i]

        result.append([-1, sum]) #guarding elems
        return result
    
    def _next_word(self, dist_list):
        if len(dist_list) <= 1:
            return None
        sum = dist_list[-1][1] #total sum is on the guard elem
        rand_point = numpy.random.randint(sum)
        #retrieve a random label from dist_list
        for i,item in enumerate(dist_list[1:]):
            if item[1] > rand_point:
                return item[0]
            
    def output_sentence(self, label, verbose=False):
        result = []
        while label != self._S_END:
            dist_list = self._get_dist_list(label)
            next_term = self._next_word(dist_list)
            if next_term == None:
                #print result, next_term, type(result), type(next_term)
                return result #special case where can't find word in dictionary
            if verbose==True:
                print 'debug: ', label, next_term, self._features[next_term]
            label = self._features[next_term].split(' ')[1]
            result += [label]
            
        return result
    
    def response(self, input_sentence):
        word_list = input_sentence.lower().split(' ')
        rand_word = numpy.random.choice(word_list)
        response = rand_word + ' ' + ' '.join(self.output_sentence(rand_word))
        return response
       
        
        
        
        


