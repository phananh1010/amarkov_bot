# Markov bot #

This is Markov bot program, an implementation of Markov chain in which the transition matrix represent the probability P(word_i|word_{i-1}) is learned from a specified corpus. The program try to output a random sequence given a fixed length.